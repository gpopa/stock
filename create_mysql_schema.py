import pymysql as mdb

"""Creating db schema for Stock app.
It should be run only once.
If foreign keys will be crated, manual cleanup should be done before running again.
Manual cleanup consist in dropping tables in reverse order than creating them."""


# Authentication credenstials
host = "localhost"
passwd = "pass"
port = 3306
user = "root"
dbname = "devdb"




con = mdb.connect(host, user, passwd)
cur = con.cursor()

cur.execute("""SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = 'devdb'""")
if cur.fetchall():
    con = mdb.connect(host, user, passwd, dbname)
else:
    con.cursor().execute('create database devdb')
    con = mdb.connect(host, user, passwd, dbname)


cur = con.cursor()
cur.execute("DROP TABLE IF EXISTS category")
cur.execute("CREATE TABLE category(idC INT NOT NULL AUTO_INCREMENT PRIMARY KEY, denC VARCHAR(255))")

cur.execute("DROP TABLE IF EXISTS product")
cur.execute("CREATE TABLE product(idP INT NOT NULL AUTO_INCREMENT PRIMARY KEY,idC INT NOT NULL, \
            denP VARCHAR(255),price DECIMAL(8,2) DEFAULT 0,\
            FOREIGN KEY (idC) REFERENCES category(idC) ON UPDATE CASCADE ON DELETE RESTRICT)")

cur.execute("DROP TABLE IF EXISTS operations")
cur.execute("CREATE TABLE operations(idO INT NOT NULL AUTO_INCREMENT PRIMARY KEY,idP INT NOT NULL,\
            qty DECIMAL(10,3) DEFAULT 0,date DATETIME)")

cur.execute("DROP TABLE IF EXISTS limits")
cur.execute("CREATE TABLE limits(idL INT NOT NULL AUTO_INCREMENT PRIMARY KEY,idP INT NOT NULL,\
            qty_limit DECIMAL(10,3) DEFAULT 0,\
            FOREIGN KEY (idP) REFERENCES product(idP) ON UPDATE CASCADE ON DELETE RESTRICT)")

con.commit()

cur.close()



