from flask import Flask, render_template, request
from db_helper import db
import time
import helper


app = Flask(__name__)


database=db()
mysql_con,mysql_cur=database.cursor()

@app.route('/', methods=['GET', 'POST'])
def main():
    """Rendering main page."""
    return render_template('index.html')

@app.route('/addProduct', methods=['GET', 'POST'])
def addProduct():
    """Rendering Add Product page.
    Managing content by request type."""
    # get cetegories from db to display availables category ids
    mysql_cur.execute("SELECT * from category")
    categories = mysql_cur.fetchall()

    if request.method == 'GET':
        return render_template('addProduct.html',categories=categories)

    if request.method == "POST":
        # get data from request
        idCat=request.form['idC']
        productName=request.form['denP']
        productPrice=request.form['price']

        # add data to db
        mysql_cur.execute("""INSERT INTO product(idC,denP,price) VALUES("%s","%s","%s")""" % (idCat,productName,productPrice))
        mysql_con.commit()

        return render_template('index.html')

@app.route('/addCategory', methods=['GET', 'POST'])
def addCategory():
    """Rendering Add Category page.
        Managing content by request type."""

    #get cetegories from db to display availables category ids
    mysql_cur.execute("SELECT * from category")
    categories = mysql_cur.fetchall()

    if request.method=="GET":
        return render_template('addCategory.html', categories=categories)

    if request.method=="POST":
        # get data from request
        categoryName=request.form['denC']

        # add data to db
        mysql_cur.execute("""INSERT INTO category(denC) VALUES("%s")""" %(categoryName))
        mysql_con.commit()

        return render_template('index.html')

@app.route('/stockChange', methods=['GET','POST'])
def stockChange():
    """Rendering Add Stock Change page.
        Managing content by request type."""
    #get cetegories from db to display availables category ids
    mysql_cur.execute("SELECT * from category")
    categories = mysql_cur.fetchall()

    # get products from db to display availables product ids
    mysql_cur.execute("SELECT * from product")
    products = mysql_cur.fetchall()

    if request.method=="GET":
        return render_template('stockChange.html', categories=categories,products=products)
    if request.method=="POST":
        #get data from request
        productId=request.form['idP']
        quantity=request.form['qty']
        price=request.form['price']

        #get current price prom db
        mysql_cur.execute("""SELECT price from product WHERE idP= "%s" """ %(productId))
        old_price=mysql_cur.fetchall()[0][0]

        #calculate new price
        new_price=(float(price)+float(old_price))/2
        op_date = time.strftime('%Y-%m-%d %H:%M:%S')

        #add data to db
        mysql_cur.execute("""INSERT INTO operations( idP,qty,date) VALUES("%s","%s","%s")""" % (productId, quantity,op_date))
        mysql_cur.execute("""UPDATE product SET price = "%s" WHERE idP = "%s" """ %(str(new_price),productId))
        mysql_con.commit()

        return render_template('index.html')

@app.route('/setStockLimit', methods=['GET','POST'])
def setStockLimit():
    """Rendering Add Stock Limit page.
        Managing content by request type."""

    # get products from db to display availables product ids
    mysql_cur.execute("SELECT * from product")
    products = mysql_cur.fetchall()
    if request.method=="GET":
        return render_template('setStockLimit.html', products=products)
    if request.method=="POST":
        # get data from request
        productId = request.form['idP']
        limit = request.form['limit']

        # add data to db
        mysql_cur.execute("""INSERT INTO limits( idP,qty_limit) VALUES("%s","%s")""" % (productId, limit))
        mysql_con.commit()
        helper.stockMonitorStart()

        return render_template('index.html')


@app.route('/searchStock', methods=['GET','POST'])
def searchStock():
    """Rendering Search Stock page.
        Managing content by request type."""
    if request.method=="GET":
        return render_template('searchStock.html')
    if request.method=="POST":
        productName=None
        transcationQty=None

        try:
            productName=request.form['denP']

        except:
            pass
        try:
            transcationQty=request.form['qty']
        except:
            pass
        productsInfo=[]
        transactions=[]

        if productName:
            searchMatches=helper.searchForProduct(productName)
            for match in searchMatches:
                idP=database.getProductId(match)
                productsInfo.append([match,database.getCurrentStock(idP),database.getPrice(match)])

        if transcationQty:
            transactions=helper.searchForTrasnaction(transcationQty)


        return render_template('searchStock.html', productsInfo=productsInfo,transactions=transactions)

@app.route('/stockHistoryForProduct', methods=['GET','POST'])
def stockHistotyForProduct():
    """Rendering Search Stock History page.
        Managing content by request type."""

    # get products from db to display availables product ids
    mysql_cur.execute("SELECT * from product")
    products = mysql_cur.fetchall()

    if request.method=="GET":
        return render_template('stockHistoryForProduct.html', products=products)
    if request.method=="POST":
        idP=request.form['idP']
        svg=helper.generateStockHistory(idP)
        return render_template('stockHistoryForProduct.html', products=products, svg=svg)


if __name__=='__main__':
    app.run(host='0.0.0.0', debug=True, ssl_context=('cert.pem', 'key.pem'))