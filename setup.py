from setuptools import setup

setup(
    name='proiect_curs',
    version='1.0',
    packages=['venv.Lib.site-packages.flask', 'venv.Lib.site-packages.flask.json', 'venv.Lib.site-packages.pygal',
              'venv.Lib.site-packages.pygal.maps', 'venv.Lib.site-packages.pygal.test',
              'venv.Lib.site-packages.pygal.graph', 'venv.Lib.site-packages.pymysql',
              'venv.Lib.site-packages.pymysql.constants'],
    url='https://bitbucket.org/gpopa/stock/src/master/',
    license='',
    author='gpopa',
    author_email='george070393@gmail.com',
    description='App for managing a warehouse stock'
)
