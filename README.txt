Prerequisites:
 1. mySQL server running
 2. create_mysql_schema.py should be updated with the right credentials for db connection
 3. db_helper.py should be updated with the right credentials for db connection
 4. from cmd run "python3 setup.py install"
 5. manually run only once create_mysql_schema.py ("python3 create_mysql_schema.py" from cmd)
 6. start app by running app.py

Web GUI will be accessible at https://localhost:5000
Note: Certificates are self-signed and should be accepted manually from browser.
