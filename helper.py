import tempfile
import db_helper
import os
import time
import smtplib
import re
import pygal


database=db_helper.db()
mysql_con,mysql_cur=database.cursor()


def generateHTML(list):
    """Generate HTML file from product Info."""
    tmp=tempfile.mktemp(suffix='.html', dir=os.getcwd() )

    html_file=open(tmp,'w+')
    html_file.write("<table>")
    # column headers
    html_file.write("<tr>")
    html_file.write("<td><center>Operation ID</center></td>")
    html_file.write("<td><center>Product Name</center></td>")
    html_file.write("<td><center>Entry qty</center></td>")
    html_file.write("<td><center>Exit qty</center></td>")
    html_file.write("<td><center>Date</center></td>")
    html_file.write("</tr>")



    for i in range(len(list)-2):

        idO = list[i]['idO']
        denP = list[i]['denP']
        in_op=list[i]['in']
        out_op=list[i]['out']
        date = list[i]['date']

        html_file.write("<tr>")
        html_file.write("<td><center>%s</center></td>" % idO)
        html_file.write("<td><center>%s</center></td>" % denP)
        html_file.write("<td><center>%s</center></td>" % in_op)
        html_file.write("<td><center>%s</center></td>" % out_op)
        html_file.write("<td><center>%s</center></td>" % date)
        html_file.write("</tr>")

    # end the table
    html_file.write("<tr><td></td><td><center>Total In</center></td><td><center>%s</center></td><td></td></tr>" % list[-2]['in'])
    html_file.write("<tr><td></td><td><center>Total Out</center></td><td></td><td><center>%s</center></td></tr>" % list[-2]['out'])
    html_file.write(
        "<tr><td></td><td><center>Current Stock</center></td><td><center>%s</center></td><td></td></tr>" % list[-1]['in'])

    html_file.write("</table>")

    html_file.close()


    return tmp


def stockMonitor(idP):
    """Function for monitoring stock value.
     If current stock value is smaller than limit value stored in db rise email alert."""
    mysql_cur.execute("""SELECT qty_limit from limits WHERE idP="%s" """ %(idP))
    limit = mysql_cur.fetchall()[0][0]
    stock=database.getCurrentStock(idP)
    if stock< limit:
        sendAlert(idP)


def stockMonitorStart():
    """Queries db once per hour for updating stock limit values for stock monitoring."""
    while True:
        mysql_cur.execute("SELECT * from limits")
        limits=mysql_cur.fetchall()
        for limit in limits:
            stockMonitor(limit[1])
        time.sleep(3600)



def sendAlert(idP):
    """Sending alert email if stock value exceed limit."""
    attachment=generateHTML(database.getProductInfo(idP))
    mysql_cur.execute("""SELECT denP from product WHERE idP= "%s" """ % (idP))
    prodName = mysql_cur.fetchall()[0][0]

    expeditor = 'info@infoacademy.eu'
    destinatar = 'paul@fratila.eu'
    username = 'info@infoacademy.eu'
    parola = 'password'
    mesaj = """From: Stock Monitor <stockmonitor@infoacademy.eu>
    To: Paul <paul@fratila.eu>
    Subject: Stock alert for product %s

    Hello Paul,

    Stock value for %s is under limit!

    Stock Monitor!
    """%(prodName,prodName)

    try:
        smtp_ob = smtplib.SMTP('mail.infoacademy.eu:25')  #
        smtp_ob.login(username, parola)
        smtp_ob.sendmail(expeditor, destinatar, mesaj,attachment)
        print('Email sent succesfully!')

    except:
        print('Error sending email!')
    os.remove(attachment)


def searchForProduct(searchString):
    """Search stock product names based on a search string"""
    productMatches=[]
    products=database.getAllProductFromStock()
    for product in products:
        if re.findall(r"%s"%searchString,str(product).lower()):
            productMatches.append(product)
    return productMatches


def searchForTrasnaction(searchString):
    """Search stock products transactions based on a search string"""
    transactionMatches=[]
    transactions=database.getAllStockTransactions()
    for transaction in transactions:
        if re.findall(r"^%s$" % searchString, str(transaction[2])):
            productName=database.getProductName(transaction[1])
            transactionMatches.append([transaction[0],productName,transaction[2],transaction[3]])
    return transactionMatches



def generateStockHistory(idP):
    """Generating stock overview for a specific product"""
    productName=database.getProductName(idP)
    transactions = database.getAllTransactionsForPRoduct(idP)
    dates=[]
    qtys=[]
    for transaction in transactions:
        dates.append(transaction[3])
        qtys.append(transaction[2])

    date_chart = pygal.Bar(x_label_rotation=20)
    date_chart.no_data_text = "Fara Date"
    date_chart.title = 'Product Name: %s'%productName
    date_chart.human_readable = True
    date_chart.y_title = 'Operation quantity'
    date_chart.x_title = 'Operation Date'
    date_chart.x_labels = dates
    date_chart.add("Qty", qtys)
    svg=date_chart.render()
    return svg




if __name__ == "__main__":
    pass
