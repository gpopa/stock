import pymysql as mdb

class db():
    """DB connector, authentification credentials for mySQL instance should be provided"""
    def __init__(self,dbname="devdb"):
        """Conncecting to mySQL instance."""
        self.host = "localhost"
        self.passwd = "pass"
        self.port = 3306
        self.user = "root"
        self.dbname=dbname
        self.con = mdb.connect(self.host, self.user, self.passwd, self.dbname)

    def cursor(self):
        """Creating db currsor for exectuine querries."""
        return self.con,self.con.cursor()


    def getProductInfo(self,idP):
        """Provoding in/out operations for a specific product identified by argument(idP)"""
        cur=self.con.cursor()
        cur.execute("""SELECT * from operations WHERE idP= "%s" """ % (idP))
        results=cur.fetchall()
        cur.execute("""SELECT denP from product WHERE idP= "%s" """ % (idP))
        prodName=cur.fetchall()[0][0]
        prod_info=[]
        total_in=0
        total_out=0
        for op in results:
            if float(op[2])>=0:
                total_in+=float(op[2])
                prod_info.append({'idO':op[0],'denP':prodName,'in':float(op[2]),'out':float(0),'date':op[3]})
            elif float(op[2])<0:
                total_out += float(op[2])
                prod_info.append({'idO': op[0], 'denP': prodName, 'in': float(0), 'out':float(op[2]), 'date':op[3]})
        prod_info.append({'idO': "", 'denP': "", 'in': total_in, 'out': total_out, 'date': ""})
        prod_info.append({'idO': "", 'denP': "", 'in': total_in + total_out, 'out': '', 'date': ""})

        return prod_info

    def getCurrentStock(self, idP):
        """Providing current stock value for a specific product identified by argument(idP)"""
        cur = self.con.cursor()
        cur.execute("""SELECT * from operations WHERE idP= "%s" """ % (idP))
        results = cur.fetchall()
        total_in = 0
        total_out = 0
        for op in results:
            if float(op[2]) >= 0:

                total_in += float(op[2])
            elif float(op[2]) < 0:
                total_out += float(op[2])
        stock = total_in + total_out

        return stock

    def getProductId(self,productName):
        """Returning db equivalent idP for a product name."""
        cur = self.con.cursor()
        cur.execute("""SELECT idP from product WHERE denP= "%s" """ % (productName))
        results = cur.fetchall()[0][0]
        return results

    def getProductName(self,idP):
        """Returning db equivalent denP for a product ID."""
        cur = self.con.cursor()
        cur.execute("""SELECT denP from product WHERE idP= "%s" """ % (idP))
        results = cur.fetchall()[0][0]
        return results

    def getPrice(self,productName):
        """Providing current price value for a specific product identified by argument(productName)"""
        idP= self.getProductId(productName)
        cur = self.con.cursor()
        cur.execute("""SELECT price from product WHERE idP= "%s" """ % (idP))
        results = cur.fetchall()[0][0]
        return results


    def getAllProductFromStock(self):
        """Returning all products from stock."""
        cur = self.con.cursor()
        cur.execute("SELECT denP from product " )
        results = cur.fetchall()
        products=[]
        for i in results:
            products.append(i[0])
        return products

    def getAllStockTransactions(self):
        """Returning all transactions in/out from stock."""
        cur = self.con.cursor()
        cur.execute("SELECT * from operations " )
        results = cur.fetchall()
        return results

    def getAllTransactionsForPRoduct(self,idP):
        """Returning all transactions in/out for a speciffic product identified by argument(idP)."""
        cur = self.con.cursor()
        cur.execute("""SELECT * from operations WHERE idP="%s" """ %idP)
        results = cur.fetchall()
        return results


if __name__ == "__main__":
    pass